unix = System.system_time(:seconds)
dt = DateTime.from_unix!(unix)
timezone = "America/New_York"

Benchee.run(%{
  fast_local_datetime: fn ->
    FastLocalDatetime.unix_to_datetime(unix, timezone)
  end,
  timex: fn ->
    unix
    |> DateTime.from_unix!()
    |> Timex.to_datetime(timezone)
  end,
  timex_only: fn ->
    Timex.to_datetime(dt, timezone)
  end,
  calendar: fn ->
    unix
    |> DateTime.from_unix!()
    |> Calendar.DateTime.shift_zone!(timezone)
  end,
  calendar_only: fn ->
    Calendar.DateTime.shift_zone!(dt, timezone)
  end
})
